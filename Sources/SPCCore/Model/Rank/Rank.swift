import Foundation

/// The `Rank` of a Standard Playing Card
///
/// Ace is low; King is high
public enum Rank: Int, CaseIterable {
    case ace = 1
    case two
    case three
    case four
    case five
    case six
    case seven
    case eight
    case nine
    case ten
    case jack
    case queen
    case king
    
    /// A "royal" `Rank` is a Jack, Queen, or King
    public var isRoyal: Bool { (Self.jack)...(Self.king) ~= self }
}
