import Foundation

public func == (lhs: Rank, rhs: Rank) -> Bool {
    lhs.rawValue == rhs.rawValue
}

public func < (lhs: Rank, rhs: Rank) -> Bool {
    lhs.rawValue < rhs.rawValue
}

extension Rank: Comparable {}
