import Foundation

/// A Standard Deck of Playing Cards consists of one card of each Rank and Suit
public enum StandardPlayingCards {}

// MARK:- Suit Groups; Hearts
extension StandardPlayingCards {
    public static let aceOfHearts = Card(rank: Rank.ace, suit: Suit.hearts)
    public static let twoOfHearts = Card(rank: Rank.two, suit: Suit.hearts)
    public static let threeOfHearts = Card(rank: Rank.three, suit: Suit.hearts)
    public static let fourOfHearts = Card(rank: Rank.four, suit: Suit.hearts)
    public static let fiveOfHearts = Card(rank: Rank.five, suit: Suit.hearts)
    public static let sixOfHearts = Card(rank: Rank.six, suit: Suit.hearts)
    public static let sevenOfHearts = Card(rank: Rank.seven, suit: Suit.hearts)
    public static let eightOfHearts = Card(rank: Rank.eight, suit: Suit.hearts)
    public static let nineOfHearts = Card(rank: Rank.nine, suit: Suit.hearts)
    public static let tenOfHearts = Card(rank: Rank.ten, suit: Suit.hearts)
    public static let jackOfHearts = Card(rank: Rank.jack, suit: Suit.hearts)
    public static let queenOfHearts = Card(rank: Rank.queen, suit: Suit.hearts)
    public static let kingOfHearts = Card(rank: Rank.king, suit: Suit.hearts)
    
    /// The hearts of a deck of playing cards.
    public static let allHearts = [
        aceOfHearts,
        twoOfHearts,
        threeOfHearts,
        fourOfHearts,
        fiveOfHearts,
        sixOfHearts,
        sevenOfHearts,
        eightOfHearts,
        nineOfHearts,
        tenOfHearts,
        jackOfHearts,
        queenOfHearts,
        kingOfHearts
    ]
}

// MARK:- Suit Groups; Diamonds
extension StandardPlayingCards {
    public static let aceOfDiamonds = Card(rank: Rank.ace, suit: Suit.diamonds)
    public static let twoOfDiamonds = Card(rank: Rank.two, suit: Suit.diamonds)
    public static let threeOfDiamonds = Card(rank: Rank.three, suit: Suit.diamonds)
    public static let fourOfDiamonds = Card(rank: Rank.four, suit: Suit.diamonds)
    public static let fiveOfDiamonds = Card(rank: Rank.five, suit: Suit.diamonds)
    public static let sixOfDiamonds = Card(rank: Rank.six, suit: Suit.diamonds)
    public static let sevenOfDiamonds = Card(rank: Rank.seven, suit: Suit.diamonds)
    public static let eightOfDiamonds = Card(rank: Rank.eight, suit: Suit.diamonds)
    public static let nineOfDiamonds = Card(rank: Rank.nine, suit: Suit.diamonds)
    public static let tenOfDiamonds = Card(rank: Rank.ten, suit: Suit.diamonds)
    public static let jackOfDiamonds = Card(rank: Rank.jack, suit: Suit.diamonds)
    public static let queenOfDiamonds = Card(rank: Rank.queen, suit: Suit.diamonds)
    public static let kingOfDiamonds = Card(rank: Rank.king, suit: Suit.diamonds)
    
    /// The diamonds of a deck of playing cards.
    public static let allDiamonds = [
        aceOfDiamonds,
        twoOfDiamonds,
        threeOfDiamonds,
        fourOfDiamonds,
        fiveOfDiamonds,
        sixOfDiamonds,
        sevenOfDiamonds,
        eightOfDiamonds,
        nineOfDiamonds,
        tenOfDiamonds,
        jackOfDiamonds,
        queenOfDiamonds,
        kingOfDiamonds
    ]
}

// MARK:- Suit Groups; Spades
extension StandardPlayingCards {
    public static let aceOfSpades = Card(rank: Rank.ace, suit: Suit.spades)
    public static let twoOfSpades = Card(rank: Rank.two, suit: Suit.spades)
    public static let threeOfSpades = Card(rank: Rank.three, suit: Suit.spades)
    public static let fourOfSpades = Card(rank: Rank.four, suit: Suit.spades)
    public static let fiveOfSpades = Card(rank: Rank.five, suit: Suit.spades)
    public static let sixOfSpades = Card(rank: Rank.six, suit: Suit.spades)
    public static let sevenOfSpades = Card(rank: Rank.seven, suit: Suit.spades)
    public static let eightOfSpades = Card(rank: Rank.eight, suit: Suit.spades)
    public static let nineOfSpades = Card(rank: Rank.nine, suit: Suit.spades)
    public static let tenOfSpades = Card(rank: Rank.ten, suit: Suit.spades)
    public static let jackOfSpades = Card(rank: Rank.jack, suit: Suit.spades)
    public static let queenOfSpades = Card(rank: Rank.queen, suit: Suit.spades)
    public static let kingOfSpades = Card(rank: Rank.king, suit: Suit.spades)
    
    /// The spades of a deck of playing cards.
    public static let allSpades = [
        aceOfSpades,
        twoOfSpades,
        threeOfSpades,
        fourOfSpades,
        fiveOfSpades,
        sixOfSpades,
        sevenOfSpades,
        eightOfSpades,
        nineOfSpades,
        tenOfSpades,
        jackOfSpades,
        queenOfSpades,
        kingOfSpades
    ]
}

// MARK:- Suit Groups; Clubs
extension StandardPlayingCards {
    public static let aceOfClubs = Card(rank: Rank.ace, suit: Suit.clubs)
    public static let twoOfClubs = Card(rank: Rank.two, suit: Suit.clubs)
    public static let threeOfClubs = Card(rank: Rank.three, suit: Suit.clubs)
    public static let fourOfClubs = Card(rank: Rank.four, suit: Suit.clubs)
    public static let fiveOfClubs = Card(rank: Rank.five, suit: Suit.clubs)
    public static let sixOfClubs = Card(rank: Rank.six, suit: Suit.clubs)
    public static let sevenOfClubs = Card(rank: Rank.seven, suit: Suit.clubs)
    public static let eightOfClubs = Card(rank: Rank.eight, suit: Suit.clubs)
    public static let nineOfClubs = Card(rank: Rank.nine, suit: Suit.clubs)
    public static let tenOfClubs = Card(rank: Rank.ten, suit: Suit.clubs)
    public static let jackOfClubs = Card(rank: Rank.jack, suit: Suit.clubs)
    public static let queenOfClubs = Card(rank: Rank.queen, suit: Suit.clubs)
    public static let kingOfClubs = Card(rank: Rank.king, suit: Suit.clubs)
    
    /// The clubs of a deck of playing cards.
    public static let allClubs = [
        aceOfClubs,
        twoOfClubs,
        threeOfClubs,
        fourOfClubs,
        fiveOfClubs,
        sixOfClubs,
        sevenOfClubs,
        eightOfClubs,
        nineOfClubs,
        tenOfClubs,
        jackOfClubs,
        queenOfClubs,
        kingOfClubs
    ]
}

// MARK:- Rank Groups
extension StandardPlayingCards {
    /// The Aces of a deck of playing cards.
    public static let allAces: [Card] = [aceOfHearts, aceOfDiamonds, aceOfSpades, aceOfClubs]
    
    /// The Twos of a deck of playing cards.
    public static let allTwos: [Card] = [twoOfHearts, twoOfDiamonds, twoOfSpades, twoOfClubs]
    
    /// The Threes of a deck of playing cards.
    public static let allThrees: [Card] = [threeOfHearts, threeOfDiamonds, threeOfSpades, threeOfClubs]
    
    /// The Fours of a deck of playing cards.
    public static let allFours: [Card] = [fourOfHearts, fourOfDiamonds, fourOfSpades, fourOfClubs]
    
    /// The Fives of a deck of playing cards.
    public static let allFives: [Card] = [fiveOfHearts, fiveOfDiamonds, fiveOfSpades, fiveOfClubs]
    
    /// The Sixes of a deck of playing cards.
    public static let allSixes: [Card] = [sixOfHearts, sixOfDiamonds, sixOfSpades, sixOfClubs]
    
    /// The Sevens of a deck of playing cards.
    public static let allSevens: [Card] = [sixOfHearts, sixOfDiamonds, sixOfSpades, sixOfClubs]
    
    /// The Eights of a deck of playing cards.
    public static let allEights: [Card] = [eightOfHearts, eightOfDiamonds, eightOfSpades, eightOfClubs]
    
    /// The Nines of a deck of playing cards.
    public static let allNines: [Card] = [nineOfHearts, nineOfDiamonds, nineOfSpades, nineOfClubs]
    
    /// The Tens of a deck of playing cards.
    public static let allTens: [Card] = [tenOfHearts, tenOfDiamonds, tenOfSpades, tenOfClubs]
    
    /// The Jacks of a deck of playing cards.
    public static let allJacks: [Card] = [jackOfHearts, jackOfDiamonds, jackOfSpades, jackOfClubs]
    
    /// The Queens of a deck of playing cards.
    public static let allQueens: [Card] = [queenOfHearts, queenOfDiamonds, queenOfSpades, queenOfClubs]
    
    /// The Kings of a deck of playing cards.
    public static let allKings: [Card] = [kingOfHearts, kingOfDiamonds, kingOfSpades, kingOfClubs]
}

// MARK:- Non-Rank, Non-Suit Card Groups
extension StandardPlayingCards {
    /// A full deck of playing cards.
    public static let allCards: [Card] = [allHearts, allDiamonds, allSpades, allClubs].flatMap({ $0 })
    
    /// The royals of a deck of playing cards.
    public static let allRoyals: [Card] = [
        jackOfHearts,
        queenOfHearts,
        kingOfHearts,
        jackOfDiamonds,
        queenOfDiamonds,
        kingOfDiamonds,
        jackOfSpades,
        queenOfSpades,
        kingOfSpades,
        jackOfClubs,
        queenOfClubs,
        kingOfClubs
    ]
    
    /// The non-royals of a deck of playing cards.
    public static let allNonRoyals: [Card] = [
        aceOfHearts,
        twoOfHearts,
        threeOfHearts,
        fourOfHearts,
        fiveOfHearts,
        sixOfHearts,
        sevenOfHearts,
        eightOfHearts,
        nineOfHearts,
        tenOfHearts,
        aceOfDiamonds,
        twoOfDiamonds,
        threeOfDiamonds,
        fourOfDiamonds,
        fiveOfDiamonds,
        sixOfDiamonds,
        sevenOfDiamonds,
        eightOfDiamonds,
        nineOfDiamonds,
        tenOfDiamonds,
        aceOfSpades,
        twoOfSpades,
        threeOfSpades,
        fourOfSpades,
        fiveOfSpades,
        sixOfSpades,
        sevenOfSpades,
        eightOfSpades,
        nineOfSpades,
        tenOfSpades,
        aceOfClubs,
        twoOfClubs,
        threeOfClubs,
        fourOfClubs,
        fiveOfClubs,
        sixOfClubs,
        sevenOfClubs,
        eightOfClubs,
        nineOfClubs,
        tenOfClubs
    ]
}
