import Foundation

extension Suit: ShorthandStringConvertible {
    /// The short description of a `Suit` (single character)
    public var shorthand: String {
        get {
            switch self {
            case .hearts: return "H"
            case .diamonds: return "D"
            case .spades: return "S"
            case .clubs: return "C"
            }
        }
    }
}
