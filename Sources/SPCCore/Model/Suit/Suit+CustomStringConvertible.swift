import Foundation

extension Suit: CustomStringConvertible {
    /// The long description of a `Suit` (lower case)
    public var description: String {
        get {
            switch self {
            case .hearts: return "hearts"
            case .diamonds: return "diamonds"
            case .spades: return "spades"
            case .clubs: return "clubs"
            }
        }
    }
}
