import Foundation

/// A `Suit` is either a Heart, Diamond, Spade, or Club
public enum Suit: CaseIterable, Equatable {
    case hearts
    case diamonds
    case spades
    case clubs
}
