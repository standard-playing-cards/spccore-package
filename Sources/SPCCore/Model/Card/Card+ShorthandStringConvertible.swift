import Foundation

extension Card: ShorthandStringConvertible {
    /// A short description of a Card in the form of "'R''S'"
    ///
    /// ie: "AS", "QH"
    public var shorthand: String {
        "\(rank.shorthand)\(suit.shorthand)"
    }
}
