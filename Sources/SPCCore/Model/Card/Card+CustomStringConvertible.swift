import Foundation

extension Card: CustomStringConvertible {
    /// A long description of a Card in the form of "'Rank' of 'Suit'"
    ///
    /// ie: "ace of spades", "queen of hearts"
    public var description: String {
        "\(rank.description) of \(suit.description)"
    }
}
