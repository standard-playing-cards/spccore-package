import Foundation

/// A Card consists of a Rank and a Suit
public struct Card: Equatable {
    /// The Rank of the Card
    ///
    /// Ace, 1, 2, ..., Jack, Queen, King
    public let rank: Rank
    
    /// The Suit of the Card
    ///
    /// Hearts, Diamonds, Spades, Clubs
    public let suit: Suit
    
    /// Whether or not this Card is a "royal" (Rank is Jack, Queen, or King)
    public var isRoyal: Bool { rank.isRoyal }
    
    /// Creates a `Card` with the given `Rank` and `Suit`
    /// - Parameters:
    ///   - rank: The `Rank` of the `Card`
    ///   - suit: The `Suit` of the `Card`
    public init(rank: Rank, suit: Suit) {
        self.rank = rank
        self.suit = suit
    }
}
