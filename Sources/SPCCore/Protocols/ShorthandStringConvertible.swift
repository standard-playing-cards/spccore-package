import Foundation

public protocol ShorthandStringConvertible {
    var shorthand: String { get }
}
