//
//  CardsTests.swift
//  
//
//  Created by Tim Fuqua on 4/24/22.
//

import SPCCore
import XCTest

class CardsTests: XCTestCase {
    func test_allCards() {
        // Arrange
        let expected: [Card] = [
            StandardPlayingCards.aceOfHearts,
            StandardPlayingCards.twoOfHearts,
            StandardPlayingCards.threeOfHearts,
            StandardPlayingCards.fourOfHearts,
            StandardPlayingCards.fiveOfHearts,
            StandardPlayingCards.sixOfHearts,
            StandardPlayingCards.sevenOfHearts,
            StandardPlayingCards.eightOfHearts,
            StandardPlayingCards.nineOfHearts,
            StandardPlayingCards.tenOfHearts,
            StandardPlayingCards.jackOfHearts,
            StandardPlayingCards.queenOfHearts,
            StandardPlayingCards.kingOfHearts,
            StandardPlayingCards.aceOfDiamonds,
            StandardPlayingCards.twoOfDiamonds,
            StandardPlayingCards.threeOfDiamonds,
            StandardPlayingCards.fourOfDiamonds,
            StandardPlayingCards.fiveOfDiamonds,
            StandardPlayingCards.sixOfDiamonds,
            StandardPlayingCards.sevenOfDiamonds,
            StandardPlayingCards.eightOfDiamonds,
            StandardPlayingCards.nineOfDiamonds,
            StandardPlayingCards.tenOfDiamonds,
            StandardPlayingCards.jackOfDiamonds,
            StandardPlayingCards.queenOfDiamonds,
            StandardPlayingCards.kingOfDiamonds,
            StandardPlayingCards.aceOfSpades,
            StandardPlayingCards.twoOfSpades,
            StandardPlayingCards.threeOfSpades,
            StandardPlayingCards.fourOfSpades,
            StandardPlayingCards.fiveOfSpades,
            StandardPlayingCards.sixOfSpades,
            StandardPlayingCards.sevenOfSpades,
            StandardPlayingCards.eightOfSpades,
            StandardPlayingCards.nineOfSpades,
            StandardPlayingCards.tenOfSpades,
            StandardPlayingCards.jackOfSpades,
            StandardPlayingCards.queenOfSpades,
            StandardPlayingCards.kingOfSpades,
            StandardPlayingCards.aceOfClubs,
            StandardPlayingCards.twoOfClubs,
            StandardPlayingCards.threeOfClubs,
            StandardPlayingCards.fourOfClubs,
            StandardPlayingCards.fiveOfClubs,
            StandardPlayingCards.sixOfClubs,
            StandardPlayingCards.sevenOfClubs,
            StandardPlayingCards.eightOfClubs,
            StandardPlayingCards.nineOfClubs,
            StandardPlayingCards.tenOfClubs,
            StandardPlayingCards.jackOfClubs,
            StandardPlayingCards.queenOfClubs,
            StandardPlayingCards.kingOfClubs,
        ]
        
        // Act
        // Assert
        XCTAssertEqual(StandardPlayingCards.allCards, expected)
    }
}
