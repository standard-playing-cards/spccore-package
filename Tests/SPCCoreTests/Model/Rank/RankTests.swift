import SPCCore
import XCTest

class RankTests: XCTestCase {
    func test_comparable() {
        // Arrange
        // Act
        // Assert
        for index in 0..<StandardPlayingCards.allHearts.count - 1 {
            XCTAssertTrue(StandardPlayingCards.allHearts[index].rank < StandardPlayingCards.allHearts[index + 1].rank)
        }
    }
    
    func test_isRoyal() {
        // Arrange
        // Act
        // Assert
        XCTAssertFalse(StandardPlayingCards.aceOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.twoOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.threeOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.fourOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.fiveOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.sixOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.sevenOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.eightOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.nineOfHearts.rank.isRoyal)
        XCTAssertFalse(StandardPlayingCards.tenOfHearts.rank.isRoyal)
        XCTAssertTrue(StandardPlayingCards.jackOfHearts.rank.isRoyal)
        XCTAssertTrue(StandardPlayingCards.queenOfHearts.rank.isRoyal)
        XCTAssertTrue(StandardPlayingCards.kingOfHearts.rank.isRoyal)
    }
}
