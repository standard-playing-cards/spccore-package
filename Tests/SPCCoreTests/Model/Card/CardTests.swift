import SPCCore
import XCTest

class CardTests: XCTestCase {}

// MARK: allHearts
extension CardTests {
    func test_description_allHearts() {
        // Arrange
        let expected: [String] = [
            "ace of hearts",
            "two of hearts",
            "three of hearts",
            "four of hearts",
            "five of hearts",
            "six of hearts",
            "seven of hearts",
            "eight of hearts",
            "nine of hearts",
            "ten of hearts",
            "jack of hearts",
            "queen of hearts",
            "king of hearts",
        ]
        
        // Act
        let allHeartsDescriptions = StandardPlayingCards.allHearts.map(\.description)

        // Assert
        XCTAssertEqual(allHeartsDescriptions, expected)
    }
    
    func test_shorthand_allHearts() {
        // Arrange
        let expected: [String] = [
            "AH",
            "2H",
            "3H",
            "4H",
            "5H",
            "6H",
            "7H",
            "8H",
            "9H",
            "10H",
            "JH",
            "QH",
            "KH",
        ]
        
        // Act
        let allHeartsShorthand = StandardPlayingCards.allHearts.map(\.shorthand)

        // Assert
        XCTAssertEqual(allHeartsShorthand, expected)
    }
}

// MARK: allDiamonds
extension CardTests {
    func test_description_allDiamonds() {
        // Arrange
        let expected: [String] = [
            "ace of diamonds",
            "two of diamonds",
            "three of diamonds",
            "four of diamonds",
            "five of diamonds",
            "six of diamonds",
            "seven of diamonds",
            "eight of diamonds",
            "nine of diamonds",
            "ten of diamonds",
            "jack of diamonds",
            "queen of diamonds",
            "king of diamonds",
        ]
        
        // Act
        let allDiamondsDescriptions = StandardPlayingCards.allDiamonds.map(\.description)

        // Assert
        XCTAssertEqual(allDiamondsDescriptions, expected)
    }
    
    func test_shorthand_allDiamonds() {
        // Arrange
        let expected: [String] = [
            "AD",
            "2D",
            "3D",
            "4D",
            "5D",
            "6D",
            "7D",
            "8D",
            "9D",
            "10D",
            "JD",
            "QD",
            "KD",
        ]
        
        // Act
        let allDiamondsShorthand = StandardPlayingCards.allDiamonds.map(\.shorthand)

        // Assert
        XCTAssertEqual(allDiamondsShorthand, expected)
    }
}

// MARK: allSpades
extension CardTests {
    func test_description_allSpades() {
        // Arrange
        let expected: [String] = [
            "ace of spades",
            "two of spades",
            "three of spades",
            "four of spades",
            "five of spades",
            "six of spades",
            "seven of spades",
            "eight of spades",
            "nine of spades",
            "ten of spades",
            "jack of spades",
            "queen of spades",
            "king of spades",
        ]
        
        // Act
        let allSpadesDescriptions = StandardPlayingCards.allSpades.map(\.description)

        // Assert
        XCTAssertEqual(allSpadesDescriptions, expected)
    }
    
    func test_shorthand_allSpades() {
        // Arrange
        let expected: [String] = [
            "AS",
            "2S",
            "3S",
            "4S",
            "5S",
            "6S",
            "7S",
            "8S",
            "9S",
            "10S",
            "JS",
            "QS",
            "KS",
        ]
        
        // Act
        let allSpadesShorthand = StandardPlayingCards.allSpades.map(\.shorthand)

        // Assert
        XCTAssertEqual(allSpadesShorthand, expected)
    }
}

// MARK: allClubs
extension CardTests {
    func test_description_allClubs() {
        // Arrange
        let expected: [String] = [
            "ace of clubs",
            "two of clubs",
            "three of clubs",
            "four of clubs",
            "five of clubs",
            "six of clubs",
            "seven of clubs",
            "eight of clubs",
            "nine of clubs",
            "ten of clubs",
            "jack of clubs",
            "queen of clubs",
            "king of clubs",
        ]
        
        // Act
        let allClubsDescriptions = StandardPlayingCards.allClubs.map(\.description)

        // Assert
        XCTAssertEqual(allClubsDescriptions, expected)
    }
    
    func test_shorthand_allClubs() {
        // Arrange
        let expected: [String] = [
            "AC",
            "2C",
            "3C",
            "4C",
            "5C",
            "6C",
            "7C",
            "8C",
            "9C",
            "10C",
            "JC",
            "QC",
            "KC",
        ]
        
        // Act
        let allClubsShorthand = StandardPlayingCards.allClubs.map(\.shorthand)

        // Assert
        XCTAssertEqual(allClubsShorthand, expected)
    }
}

// MARK: isRoyal
extension CardTests {
    func test_isRoyal() {
        // Arrange
        // Act
        // Assert
        for card in StandardPlayingCards.allNonRoyals {
            XCTAssertFalse(card.isRoyal)
        }

        for card in StandardPlayingCards.allRoyals {
            XCTAssertTrue(card.isRoyal)
        }
    }
}
