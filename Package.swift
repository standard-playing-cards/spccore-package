// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SPCCore",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "SPCCore",
            targets: ["SPCCore"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "SPCCore",
            dependencies: []
        ),
        .testTarget(
            name: "SPCCoreTests",
            dependencies: ["SPCCore"]),
    ]
)
